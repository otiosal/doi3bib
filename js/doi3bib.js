async function fetchFromCrossref(doi) {
  // Attempts to fetch bibtex info from crossref.org
  let url = 'https://api.crossref.org/works/' + doi + '/transform/application/x-bibtex';
  let response = await fetch(url);

  let status = await response.status;

  if (status == 200) {
    let data = await response.text();
    return formatBibEntry(data);
  } else if (status == 404) {
    return "We could not find this resource. Check that the DOI is entered correctly. \
      <br>Please open an issue on GitLab with the DOI if you think this is a mistake."
  } else {
    "There was an issue. Please try again, it is possible the servers are recieving too many requests. \
    <br>If this problem persistes open an issue with your search parameters on GitLab"
  }
}

async function fetchFromDOI(doi) {
  // Attempts to fetch bibtex info from doi.org
  let url = 'https://dx.doi.org/' + doi;
  let response = await fetch(url, {
    headers: {
      'Accept': 'application/x-bibtex'
    }
  })
  let status = await response.status;

  if (status == 200) {
    let data = await response.text();
    return formatBibEntry(data);
  } else {
    return fetchFromCrossref(doi)
  }
}

async function fetchFromPMID(pmid) {
  // Fetches DOI from NIH and redirects to DOI search.
  let response = await fetch('https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?format=json&ids=' + pmid);
  let status = await response.status;

  if (status == 200) {
    let data = await response.json();
    let doi = data.records[0].doi;
    return fetchFromDOI(doi);
  } else {
    return "We could not find this resource. Check that the PMID is entered correctly. \
      <br>Please open an issue on GitLab with the PMID if you think this is a mistake."
  }
}

function formatBibEntry(bibentry) {
  // Ampersands
  bibentry = bibentry.replace(/\\&/g, 'and');
  bibentry = bibentry.replace(/amp/g, '');

  // Greek Letters
  bibentry = bibentry.replace(/\\up/g, '\\');
  bibentry = bibentry.replace(/\\epsilon/g, '\\varepsilon');

  // Subscripts
  var subTwo = new RegExp(/\$\\less\$sub\$\\greater\$2\$\\less\$\/sub\$\\greater\$/g);
  bibentry = bibentry.replace(subTwo, '$_2$')

  // Fringe / Extraneous
  bibentry = bibentry.replaceAll('$\\mathsemicolon$', '');
  
  // DOI & URL problems  
  bibentry = bibentry.replaceAll(/%/g, '\\%');

  // Emph
  bibentry = bibentry.replace(/\$\\less\$i\$\\greater\$(.*?)(?=\$)\$\\less\$\/i\$\\greater\$/g, '\emph{$1}')

  return bibentry
}

function redirectQuery() {
  const query = document.querySelector('input').value.trim();
  var doiReg = new RegExp(/^10./);
  if (doiReg.test(query)) {
    window.location = "?doi=" + query;
  } else {
    window.location = "?pmid=" + query;
  }
  return false;
}

function redirectAbout() {
  let params = (new URL(document.location));
  params.searchParams.delete('doi');
  params.searchParams.delete('pmid');
  window.location.replace(params.href);
  return false;
}

function copyBibEntry() {
  var range = document.createRange();
  range.selectNode(document.getElementById("bib-entry"));
  window.getSelection().removeAllRanges();
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
}

function displayBibEntry(bibentry) {
  document.getElementById("bib-head").innerHTML = "Bibtex Entry";
  document.getElementById("bib-entry").innerHTML = bibentry;
  document.getElementById("about").remove();
}

let params = (new URL(document.location)).searchParams;
let doi = params.get('doi');
let pmid = params.get('pmid');

if (doi != null) {
  fetchFromDOI(doi).then(bibentry => {
    return displayBibEntry(bibentry);
  });
} else if (pmid != null) {
  fetchFromPMID(pmid).then(bibentry => {
    return displayBibEntry(bibentry);
  });
}
